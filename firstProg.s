# Comment giving name of program and description of function
# hw1 q1 kelvin silva
#int BitCount(unsigned x) {
#
#    int bit;
#    if (x == 0)
#        return 0;
#    bit = x & 0x1;
#    return bit + BitCount(x >> 1);
#}
# Bare-bones outline of MIPS assembly language program

           .data       # variable declarations follow this line
                       # ...
														
           .text       # instructions follow this line	
																	
.globl main              # indicates start of code (first instruction to execute)
                       # ...

main: 
	xor $4, $4, $4
	addi $4, $4, 99 #test BitCount with 99 input. Make sure to breakpoint at line 18 and check register result
	addi $16, $16, 11
	addi $17, $17, 12
        jal BitCount
                       
        li	$v0, 10		    

syscall

BitCount:

	addi $sp, $sp, -40
	sw $4, 0($sp) #save argument 
	sw $16, 4($sp) #save callee register 16 - 23
	sw $17, 8($sp)
	sw $18, 12($sp)
	sw $19, 16($sp)
	sw $20, 20($sp)
	sw $21, 24($sp)
	sw $22, 28($sp)
	sw $23, 32($sp)
	sw $ra, 36($sp)
	
	
	xor $16, $16, $16 #clear register 16, use as temp variable
	xor $17, $17, $17
	xor $18, $18, $18
	
	lw $17, 0($sp)	#grab argument and save in gen. prp register
	
	beq $17, $18, IFEQ #if statement
	andi $16, $17, 1	#if false, then and by 1
	
	
	
	srl $17, $17, 1	#shift r17 by 1, reference line 38 for argument
	xor $4, $4, $4	#clear argument register
	addu $4, $4, $17	#store shifted r17 into argument
	
	jal BitCount		#call bitcount with shifted arg
	
	addu $2, $16, $2
	
	lw $4, 0($sp)	#return calle responsible registers to original value
	lw $16, 4($sp)
	lw $17, 8($sp)
	lw $18, 12($sp)
	lw $19, 16($sp)
	lw $20, 20($sp)
	lw $21, 24($sp)
	lw $22, 28($sp)
	lw $23, 32($sp)
	lw $ra, 36($sp)	#make sure to save return address inbetween calls
	addi $sp, $sp, 40
	
	j ENDFNC
  
IFEQ:
	xor $2, $2, $2
	
	lw $4, 0($sp) #return calle responsible registers to original value
	lw $16, 4($sp)
	lw $17, 8($sp)
	lw $18, 12($sp)
	lw $19, 16($sp)
	lw $20, 20($sp)
	lw $21, 24($sp)
	lw $22, 28($sp)
	lw $23, 32($sp)
	lw $ra, 36($sp) #make sure to save return address inbetween calls
	addi $sp, $sp, 40
ENDFNC:

	jr $ra

.end



									
# End of program, leave a blank line afterwards to make SPIM happy
