# Comment giving name of program and description of function
# Template.s
# Bare-bones outline of MIPS assembly language program

           .data       # variable declarations follow this line
                       # ...
														
           .text       # instructions follow this line	
																	
.globl main              # indicates start of code (first instruction to execute)
                       # ...

main: 

                       li $4, 100
                       li $4, 200
                       move $5, $4

syscall
.end

									
# End of program, leave a blank line afterwards to make SPIM happy